import sys
import argparse
import configparser

import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import expr

parser = argparse.ArgumentParser(description='Spark Ingestion')
parser.add_argument('--conf_path', metavar='c', type=str, help='conf path to ingestion')
args = parser.parse_args()

print("rodou1!!", sys.argv, args.conf_path)


spark = SparkSession.builder.getOrCreate()

def load_config(spark_context):
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.access.key", "dNntEGfPWmegOF5YljEA")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.secret.key", "CPAGBHk0XH00Smhr08r0XLxKRGYh9zENpAFFD38g")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.endpoint", "http://minio:9000")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.connection.ssl.enabled", "true")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.path.style.access", "true")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.attempts.maximum", "1")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.connection.establish.timeout", "10")
    spark_context._jsc.hadoopConfiguration().set("fs.s3a.connection.timeout", "20")

load_config(spark.sparkContext)

print("rodou2!!")
def load_ingestion_conf():
    config = configparser.ConfigParser()
    config.read(f"/opt/bitnami/spark/ingestions/{args.conf_path}")
    config_dict = {sect: dict(config.items(sect)) for sect in config.sections()}
    return config_dict

conf = load_ingestion_conf()
print("rodou3!!", f"/ingestions/{args.conf_path}", conf)

db = conf['target']['database']
table = conf['target']['table']

options = {"header": "true", "delimiter": ","}
format = "csv"
diretorio_entrada = f"s3a://teste1/arquivos/{db}/{table}/*"
diretorio_saida = f"s3a://teste1/lakehouse/{db}/{table}/"
checkpoint_location = f"s3a://teste1/_checkpoints/{db}/{table}/"




df_schema = spark.read \
    .options(**options) \
    .option("inferSchema", True) \
    .format(format) \
    .csv(diretorio_entrada)

df_schema.printSchema()

dados_csv_stream = spark.readStream \
    .schema(df_schema.schema) \
    .options(**options) \
    .format(format) \
    .csv(diretorio_entrada)

dados_transformados = dados_csv_stream.withColumn("idade_dobro", expr("idade * 2"))

def send_to_star(df, batch_id):
    print(f"countxx {df.count()}")
    import requests
    import json
    import socket

    print(f"count {df.count()}")
    ip_address = socket.gethostbyname("starrocks")
    headers = {
        #'label': f'loading_batch_{batch_id}',
        'Expect': '100-continue',
        'format': 'json',
        'strip_outer_array': 'false',
    }
    data = "\n".join(df.toJSON().collect())
    
    response = requests.put(
        f'http://{ip_address}:8040/api/{db}/{table}/_stream_load',  # só funcionou colocando a porta do backend... do frontend fazia um redirect para localhost:backendport
        headers=headers,
        data=data,
        auth=('admin', '123456')
        #auth=('root', '')
    )
    result = response.json()

    if result["Status"] in ("FAILED", "Fail"):
        raise Exception(response.text)

    print(response.text)

query = (dados_transformados.writeStream 
    .format("parquet") 
    .outputMode("append") 
    .option("path", diretorio_saida) 
    .option("checkpointLocation", checkpoint_location) 
    .foreachBatch(send_to_star) 
    .trigger(availableNow=True)
    .start()
    )

query.awaitTermination()
    