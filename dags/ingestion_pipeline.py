from __future__ import annotations

import os
from datetime import datetime
from glob import glob
import configparser

from airflow import DAG
from airflow.decorators import task, task_group
from airflow.exceptions import AirflowSkipException, AirflowException
from airflow.models.param import Param
from airflow.utils.trigger_rule import TriggerRule

from airflow.providers.mysql.operators.mysql import MySqlOperator
from airflow.providers.apache.livy.operators.livy import LivyOperator
from airflow.operators.empty import EmptyOperator

def get_confs():
    files = glob("/opt/airflow/dags/ingestions/*.ini")

    for file in files:
        config = configparser.ConfigParser()
        config.read(file)
        config_dict = {sect: dict(config.items(sect)) for sect in config.sections()}
        yield file, config_dict


def ingestion_pipeline(conf_path, conf):
    table = conf['target']["table"]

    create_table = MySqlOperator(
        task_id=f"create_table",
        sql=conf['target'].get('create_table_sql', "select 1"), dag=dag
    )

    check_table_exists = MySqlOperator(
        task_id=f"check_table_exists",
        sql=f"""
        SELECT count(*)
        FROM information_schema.tables
        WHERE table_schema = 'teste1'
        AND table_name = '{table}'
        """, dag=dag
        #WHERE table_schema = '{{ti.dag_run.conf["database"]}}'
        #AND table_name = '{{ti.dag_run.conf["table"]}}'
    )

    @task.branch(
        task_id=f"decision_create_table"
    )
    def _decision_create_table(result):
        table_exists = result[0][0] == 1

        if table_exists:
            return [f'ingestion_{table}.join', f'ingestion_{table}.processing_data']
        
        return [f'ingestion_{table}.create_table', f'ingestion_{table}.join', f'ingestion_{table}.processing_data']


    decision_create_table = _decision_create_table(check_table_exists.output)

    processing_data = LivyOperator(
        task_id=f"processing_data",
        #name="session_name_teste",  # as vezes da duplication session name
        file="/target/send2starrocks.py",
        py_files=["/target/pyspark_apps/send2starrocks/packages.zip"],
        args=[
            "--conf_path", conf_path.split("/")[-1]
        ],
        polling_interval=10
    )

    #processing_data = EmptyOperator(task_id="processing_data")

    join = EmptyOperator(
        task_id=f"join",
        trigger_rule="none_failed_min_one_success"
    )

    decision_create_table >> create_table >> join
    decision_create_table >> join
    join >> processing_data


with DAG(
    "ingestion_pipeline",
    start_date=datetime(2021, 1, 1),
    #schedule="* * * * *",
    default_args={
        "mysql_conn_id": "starrocks"
        },
    tags=["pipeline"],
    catchup=False,
    max_active_runs=1,
    doc_md=__doc__,
    #params={
    #    "database": Param("", type="string", title="Database"),
    #    "table": Param("", type="string", title="Table"),
    #},
) as dag:
    fim = EmptyOperator(task_id="fim")

    for conf_path, conf in get_confs():
        table = conf['target']['table']

        @task_group(group_id=f"ingestion_{table}")
        def current_group(conf):
            ingestion_pipeline(conf_path, conf)

        pipeline = current_group(conf)
        pipeline >> fim
    

